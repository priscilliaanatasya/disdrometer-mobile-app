import 'dart:async';
import 'dart:convert';
import 'package:disdrometer/components/helper.dart';
import 'package:disdrometer/routes/navbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:disdrometer/style/text.dart';

class SScreen extends StatefulWidget {
  @override
  _SScreenState createState() => _SScreenState();
}

var abUs = new List();

class _SScreenState extends State<SScreen> {
  Timer timer;

  List _jsondata;
  String appName = "";
  String version = "";
  String releaseDate = "";
  String desc = "";
  String contact = "";

  attributer() {
    var attributesData = [
      'app_mobile_token',
    ];
    return attributesData;
  }

  Future getAbout() async {
    var mobileToken = Helper.mobileToken();
    var url = Helper.urlApi() + "get-about-app";
    var response = await http.post(
      url,
      headers: {
        "app_mobile_token": mobileToken,
      },
    );
    try {
      _jsondata = json.decode(response.body);
      appName = _jsondata[0]["app_name"];
      version = _jsondata[0]["version"];
      releaseDate = _jsondata[0]["release_date"];
      desc = _jsondata[0]["description"];
      contact = _jsondata[0]["contact"];
    } catch (e) {
      print('error caught: $e');
      return {
        'app_name': '',
        'version': '',
        'release_date': '',
        'description': '',
        'contact': '',
      };
    }

    _setPref();
  }

  Future about() async {
    var data = await getAbout();
    var _data = data;
    return _data;
  }

  void navPage() {
    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => Navbar()),
      (Route<dynamic> route) => false,
    );
  }

  void _setPref() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setString("appName", _jsondata[0]["app_name"]);
    pref.setString("version", _jsondata[0]["version"]);
    pref.setString("releaseDate", _jsondata[0]["release_date"]);
    pref.setString("desc", _jsondata[0]["description"]);
    pref.setString("contact", _jsondata[0]["contact"]);

    navPage();
  }

  @override
  void initState() {
    about();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    // timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      fit: StackFit.expand,
      children: <Widget>[
        Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image(
                image: AssetImage('images/launcher.png'),
                width: MediaQuery.of(context).size.width / 2.5,
              ),
            ],
          ),
        ),
        Align(
          alignment: FractionalOffset.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 8.0),
            child: Text(
              "Copyright BMKG",
              style: text13,
            ),
          ),
        ),
      ],
    ));
  }
}
