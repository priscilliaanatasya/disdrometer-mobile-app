import 'dart:async';
import 'package:disdrometer/style/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:disdrometer/style/text.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Setting extends StatefulWidget {
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  //shared pref
  String appName = "";
  String version = "";
  String releaseDate = "";
  String desc = "";
  String contact = "";

  Future _abUs() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString("appName") != null) {
      setState(() {
        appName = pref.getString("appName");
        version = pref.getString("version");
        releaseDate = pref.getString("releaseDate");
        desc = pref.getString("desc");
        contact = pref.getString("contact");
      });
    } else {
      setState(() {
        appName = "";
        version = "";
        releaseDate = "";
        desc = "";
        contact = "";
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _abUs();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(10),
        child: Container(
            child: appName != null
                ? new Stack(
                    children: [
                      Container(
                        height: MediaQuery.of(context).size.height,
                        margin: EdgeInsets.all(24),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              appName,
                              style: abUs,
                            ),
                            box20,
                            Container(
                              width: MediaQuery.of(context).size.width / 2,
                              height: 5,
                              color: primaryColor,
                            ),
                            box20,
                            Row(
                              children: [
                                Image(
                                  image: AssetImage("images/disdro.jpeg"),
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 5),
                                    child: Text(
                                      desc,
                                      style: info,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: EdgeInsets.only(top: 20),
                              child: Text(
                                contact,
                                style: info2,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.all(24),
                        child: Align(
                          alignment: FractionalOffset.bottomRight,
                          child: Text(
                            version + " | " + releaseDate,
                            style: text13n,
                          ),
                        ),
                      ),
                    ],
                  )
                : Text("null")),
      ),
    );
  }
}
