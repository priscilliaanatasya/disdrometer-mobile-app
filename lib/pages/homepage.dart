import 'dart:async';
import 'dart:convert';
import 'package:disdrometer/components/helper.dart';
import 'package:disdrometer/style/clipper.dart';
import 'package:disdrometer/style/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:disdrometer/style/text.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class Homepage extends StatefulWidget {
  @override
  _HomepageState createState() => _HomepageState();
}

class _HomepageState extends State<Homepage> {
  //date
  DateTime currentBackPressTime;

  String _dateString;
  String _timeString;

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDate = _formatDate(now);
    final String formattedTime = _formatTime(now);

    setState(() {
      _dateString = formattedDate;
      _timeString = formattedTime;
    });
  }

  String _formatDate(DateTime dateTime) {
    return DateFormat("EEEE, d MMMM yyyy").format(dateTime);
  }

  String _formatTime(DateTime dateTime) {
    return DateFormat("h:mm a").format(dateTime);
  }

  //timer
  Timer timer;

  //API

  List _jsondata;
  int curahHujan = 0;
  int diameter = 0;
  int jumlahButir = 0;
  int level = 0;
  bool display;
  String _address = "";
  Image shape;
  String status_level = "";

  bool isLoading = false;

  attributes() {
    var attributesData = [
      'app_mobile_token',
    ];
    return attributesData;
  }

  Future apiLogin() async {
    var mobileToken = Helper.mobileToken();
    var url = Helper.urlApi() + "get-value-from-lat-long";
    var response = await http.post(
      url,
      headers: {
        "app_mobile_token": mobileToken,
      },
      body: {
        "lat": _lat.toString(),
        "long": _long.toString(),
      },
    );
    try {
      _jsondata = json.decode(response.body);
      curahHujan = _jsondata[0]['curah_hujan'];
      diameter = _jsondata[0]['diameter'];
      jumlahButir = _jsondata[0]['jumlah_butir'];
      display = _jsondata[0]["display_detail_ukuran"];
      _address = _jsondata[0]["location"];
      level = _jsondata[0]["level_status"];
      status_level = _jsondata[0]['STATUS'];
      levelIcon();
    } catch (e) {
      print("error caught $e");
      _address= "UNMONITORED";
      status_level = "--";
      level = 5;
      levelIcon();
      return {
        'lat': 0,
        'long': 0,
        'STATUS': '--',
        'curah_hujan': 0,
        "diameter": 0,
        "jumlah_butir": 0,
        "display_detail_ukuran": false,
        "location": "UNMONITORED",
        "level_status": 5,
      };
    }   
  }

  Future login() async {
    var data = await apiLogin();
    var _data = data;
    return _data;
  }

  //get latitude & longitude
  double _lat;
  double _long;

  _getCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);

    _lat = position.latitude;
    _long = position.longitude;
    print(_lat);
    print(_long);

    login();
  }

  levelIcon() {
    if (level == 1) {
      shape = Image(
          image: AssetImage("images/circle.png"),
          width: MediaQuery.of(context).size.width / 2);
    } else if (level == 2) {
      shape = Image(
          image: AssetImage("images/pentagon.png"),
          width: MediaQuery.of(context).size.width / 2);
    } else if (level == 3) {
      shape = Image(
          image: AssetImage("images/square.png"),
          width: MediaQuery.of(context).size.width / 2);
    } else if (level == 4) {
      shape = Image(
          image: AssetImage("images/triangle.png"),
          width: MediaQuery.of(context).size.width / 2);
    } else {
      shape = Image(
          image: AssetImage("images/close.png"),
          width: MediaQuery.of(context).size.width / 2);
    }

    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    isLoading = false;
    _timeString = _formatTime(DateTime.now());
    _dateString = _formatDate(DateTime.now());

    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    _getCurrentLocation();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      resizeToAvoidBottomPadding: true,
      body: ModalProgressHUD(
        inAsyncCall: isLoading,
        child: Stack(
          children: <Widget>[
            ClipPath(
              clipper: MyClipper(),
              child: Container(
                color: primaryColor,
                height: MediaQuery.of(context).size.height / 7,
                padding: EdgeInsets.only(top: 30),
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Text(
                    "Indikasi Bencana Hidrologi",
                    style: top,
                  ),
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 8),
                    child: _lat == null
                        ? Text(
                            "Loading",
                            style: text13,
                          )
                        : _address == null
                            ? Text(
                                "Loading",
                                style: text13,
                              )
                            : Text(
                                _address,
                                style: text30,
                              ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      _dateString,
                      style: text15n,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      _timeString,
                      style: text15n,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: level == null || shape == null
                        ? Text(
                            "Loading",
                            style: text13,
                          )
                        : Stack(
                            children: [
                              Center(
                                child: shape,
                              ),
                              Container(
                                height: MediaQuery.of(context).size.width / 2,
                                child: Center(
                                  child: Text(
                                    status_level + " ",
                                    style: black30,
                                  ),
                                ),
                              )
                            ],
                          ),
                  )
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                margin: EdgeInsets.all(24),
                child: RaisedButton(
                  color: white.withOpacity(0.5),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                  child: Text(
                    "Segarkan halaman",
                    style: black13n,
                  ),
                  onPressed: () {
                    setState(() {
                      isLoading = true;
                    });
                    _getCurrentLocation();
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
