import 'dart:async';
import 'dart:convert';
import 'package:disdrometer/components/helper.dart';
import 'package:disdrometer/style/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:disdrometer/style/text.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends State<Search> {
  //form
  TextEditingController kodeposController = TextEditingController();

  final _key = new GlobalKey<FormState>();

  //Timer
  Timer timer;

  //API
  List _jsondata;
  int curahHujan = 0;
  int diameter = 0;
  int jumlahButir = 0;
  int kodepos = 0;
  bool display;
  String _address = "";
  int level = 0;
  Image shape;
  String jsonStatus;
  String kopos;
  String msg;

  int x = 0;

  bool isLoading = false;
  attributes() {
    var attributesData = [
      'app_mobile_token',
    ];
    return attributesData;
  }

  check() async {
    final form = _key.currentState;
    if (form.validate()) {
      x = 2;
      form.save();
      pSubmit();
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }

  Future submit() async {
    x = 1;
    var mobileToken = Helper.mobileToken();
    var url = Helper.urlApi() + "get-value-from-kodepos";
    print("----------" + url);
    print("----------" + kodeposController.text);
    var response = await http.post(
      url,
      headers: {
        "app_mobile_token": mobileToken,
      },
      body: {
        "kodepos": kodeposController.text,
      },
    );
    try {
      _jsondata = json.decode(response.body);
      jsonStatus = _jsondata[0]["STATUS"];
      kopos = _jsondata[0]["kodepos"];
      msg = _jsondata[0]["msg"];
      _address = _jsondata[0]["location"];
      level = _jsondata[0]["level_status"];
    } catch (e) {
      print('error caught: $e');
      _showError();
      return {
        'STATUS': '',
        'msg': '',
        "location": "",
        "level_status": 0,
      };
    }
    print(
        "------------------------------------------------------------------------------------------");
    print(_jsondata);
    if (jsonStatus == "") {
      setState(() {
        isLoading = false;
      });
    } else {
      levelIcon();
    }
  }

  Future<void> _showError() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Terjadi Kesalahan'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Silahkan masukan kode pos yang benar.'),
              ],
            ),
          ),
          actions: <Widget>[
            RaisedButton(
              color: primaryColor,
              child: Text('Kembali ke menu pencarian'),
              onPressed: () {
                setState(() {
                  isLoading = false;
                });
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  Future pSubmit() async {
    var data = await submit();
    var _data = data;
    return _data;
  }

  levelIcon() {
    if (level == 1) {
      shape = Image(image: AssetImage("images/circle.png"), width: 20);
    } else if (level == 2) {
      shape = Image(image: AssetImage("images/pentagon.png"), width: 20);
    } else if (level == 3) {
      shape = Image(image: AssetImage("images/square.png"), width: 20);
    } else if (level == 4) {
      shape = Image(image: AssetImage("images/triangle.png"), width: 20);
    } else {
      shape = Image(image: AssetImage("images/close.png"), width: 20);
    }

    setState(() {
      isLoading = false;
    });
  }

  void onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: primaryColor,
          child: Container(
            height: MediaQuery.of(context).size.height / 5,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.white),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: new Text(
                    "Loading",
                    style: TextStyle(color: Colors.white, fontSize: 15),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void initState() {
    isLoading = false;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    // timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _key,
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            resizeToAvoidBottomPadding: false,
            appBar: AppBar(
              title: Container(
                margin: EdgeInsets.only(left: 24),
                child: Text(
                  "Search",
                ),
              ),
              backgroundColor: primaryColor,
            ),
            body: ModalProgressHUD(
              inAsyncCall: isLoading,
              child: SingleChildScrollView(
                  child: Container(
                      margin: EdgeInsets.all(24),
                      child: Column(children: [
                        Row(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width / 1.35,
                              child: TextFormField(
                                controller: kodeposController,
                                cursorColor: black,
                                style: TextStyle(color: black),
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  hintText: "Silahkan masukan kodepos disini",
                                  hintStyle: TextStyle(color: black),
                                  filled: true,
                                  fillColor: grey,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(25.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return "Tolong tuliskan kodepos";
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                            ),
                            IconButton(
                              icon: Icon(Icons.search),
                              onPressed: () async {
                                setState(() {
                                  isLoading = true;
                                });
                                check();
                              },
                            ),
                          ],
                        ),
                        Container(
                            child: _jsondata == null
                                ? Container(
                                    height: MediaQuery.of(context).size.height /
                                        1.5,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 20.0),
                                          child: Image(
                                            image: new AssetImage(
                                              'images/launcher.png',
                                            ),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 10.0),
                                          child:
                                              Text("Silahkan masukan kodepos"),
                                        )
                                      ],
                                    ),
                                  )
                                : Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.only(top: 15),
                                    padding: EdgeInsets.all(10),
                                    // decoration: BoxDecoration(
                                    //   color: Theme.of(context).primaryColor,
                                    //   borderRadius: BorderRadius.only(
                                    //     topLeft: Radius.circular(10),
                                    //     topRight: Radius.circular(10),
                                    //     bottomLeft: Radius.circular(10),
                                    //     bottomRight: Radius.circular(10),
                                    //   ),
                                    //   boxShadow: [
                                    //     BoxShadow(
                                    //       color: Theme.of(context)
                                    //           .dividerColor
                                    //           .withOpacity(0.2),
                                    //       spreadRadius: 2,
                                    //       blurRadius: 7,
                                    //       offset: Offset(0, 3),
                                    //     ),
                                    //   ],
                                    // ),
                                    child: jsonStatus == ''
                                        ? Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "Kode pos '" +
                                                    kodeposController.text +
                                                    "' tidak ditemukan",
                                                style: text15,
                                              ),
                                            ],
                                          )
                                        : Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                                Text(
                                                  _address,
                                                  style: status,
                                                ),
                                                Row(
                                                  children: [
                                                    Text(
                                                      jsonStatus + " ",
                                                      style: list1,
                                                    ),
                                                    shape,
                                                  ],
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 10.0,
                                                          bottom: 10.0),
                                                  child: Text(
                                                    "Kode pos : " + kopos,
                                                    style: text15,
                                                  ),
                                                ),
                                              ])))
                      ]))),
            )));
  }
}
