import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:disdrometer/pages/homepage.dart';
import 'package:disdrometer/pages/search.dart';
import 'package:disdrometer/pages/setting.dart';
import 'package:disdrometer/style/color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Navbar extends StatefulWidget {
  @override
  _NavbarState createState() => _NavbarState();
}

class _NavbarState extends State<Navbar> with SingleTickerProviderStateMixin {
  List<Widget> pages = [
    Homepage(),
    Search(),
    Setting(),
  ];

  int iPage = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CurvedNavigationBar(
        color: primaryColor,
        backgroundColor: Theme.of(context).canvasColor,
        buttonBackgroundColor: Theme.of(context).primaryColor,
        animationCurve: Curves.bounceInOut,
        animationDuration: Duration(milliseconds: 100),
        height: 50,
        items: <Widget>[
          Icon(
            Icons.home,
            color: Colors.white,
          ),
          Icon(
            Icons.location_searching,
            color: Colors.white,
          ),
          Icon(
            Icons.settings,
            color: Colors.white,
          ),
        ],
        onTap: (index) {
          print("$index");
          if (this.mounted) {
            setState(
              () {
                iPage = index;
              },
            );
          }
        },
      ),
      body: pages[iPage],
    );
  }
}
