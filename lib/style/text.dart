import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'color.dart';

final top = TextStyle(color: white, fontWeight: FontWeight.bold);

final black30 =
    TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: black);

final text30 = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

final text15 = TextStyle(fontSize: 15, fontWeight: FontWeight.bold);

final text15n = TextStyle(fontSize: 15, fontWeight: FontWeight.normal);

final status = TextStyle(fontSize: 20, fontWeight: FontWeight.bold);

final info = TextStyle(fontSize: 13, fontWeight: FontWeight.bold);

final info2 = TextStyle(fontSize: 13, fontWeight: FontWeight.bold);

final abUs = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);

final list1 = TextStyle(fontSize: 18, fontWeight: FontWeight.bold);

final text13 = TextStyle(fontSize: 13, fontWeight: FontWeight.bold);

final text13n = TextStyle(fontSize: 13, fontWeight: FontWeight.normal);

final black13n =
    TextStyle(fontSize: 13, color: black, fontWeight: FontWeight.normal);
