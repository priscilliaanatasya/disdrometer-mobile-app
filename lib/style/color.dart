import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final primaryColor = Color(0xff0000CC);

final white = Colors.white;

final black = Colors.black;

final grey = Colors.grey[300];

final box20 = SizedBox(height: 10);
